/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lvargas.springtut;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

/**
 *
 * @author luis
 */
public class CustomObjectMapper extends ObjectMapper {
    public CustomObjectMapper() {
        registerModule(new Hibernate4Module());
        
        configure(SerializationFeature.INDENT_OUTPUT, true);
    }
}
