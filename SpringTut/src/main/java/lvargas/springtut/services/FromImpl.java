/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lvargas.springtut.services;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.jpa.JinqJPAStreamProvider;
import org.jinq.orm.stream.JinqStream.Where;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author luis
 */
@Component
public class FromImpl implements From {

    @PersistenceContext
    private EntityManager em;

    JinqJPAStreamProvider sp;

    public <U> JPAJinqStream<U> streamAll(Class<U> clazz) {
        if(sp == null) sp = new JinqJPAStreamProvider(em.getMetamodel());
        
        return sp.streamAll(em, clazz);
    }
}
