/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lvargas.springtut.services;

import org.jinq.jpa.JPAJinqStream;

/**
 *
 * @author luis
 */
public interface From {

    public <U> JPAJinqStream<U> streamAll(Class<U> clazz);
}
