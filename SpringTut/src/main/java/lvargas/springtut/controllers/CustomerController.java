/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lvargas.springtut.controllers;

import lvargas.springtut.models.Customer;
import lvargas.springtut.services.From;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author luis
 */
@Controller
@RequestMapping("/customers")
public class CustomerController {
    
//    @Autowired
//    CustomerRepository customerRepository;

    @Autowired
    From from;
    
    @RequestMapping("/{id}")
    @ResponseBody
    public Customer findOne(@PathVariable("id") Integer id) {
        
//        Customer customer = customerRepository.findOne(id);
        
//        return customer;
        return from.streamAll(Customer.class).where(c -> c.getId() == id).getOnlyValue();
    }
}
