# Spring Tutorial

In this tutorial I will show you how to use Spring MVC, Spring Data JPA, Hibernate and Jinq together.

##Create a Component
The first step is going to be to create a component class that is going to wire the entity manager and Jinq.

First we should create the Interface:
```java
public interface From {

    public <U> JPAJinqStream<U> streamAll(Class<U> clazz);
}
```

An then we should create the implementation class
```java
@Component
public class FromImpl implements From {

    @PersistenceContext
    private EntityManager em; //Here spring injects entity manager to this component

    JinqJPAStreamProvider streams;

    public <U> JPAJinqStream<U> streamAll(Class<U> clazz) {
        return new JinqJPAStreamProvider(em.getMetamodel()).streamAll(em, clazz);
    }
}
```

Then you can use this component easy in other spring components (Controllers, Services, Repositories, etc...). For example to use in a Controller you only need to do:
```Java
@Controller
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    From from;
    
    @RequestMapping("/{id}")
    @ResponseBody
    public Customer findOne(@PathVariable("id") Integer id) {
        return from.streamAll(Customer.class).where(c -> c.getId() == id).getOnlyValue();
    }
}
```